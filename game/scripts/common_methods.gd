extends Node
class_name CommonMethods


static func vector2i_to_array(vector2i : Vector2i) -> Array:
	return [vector2i.x, vector2i.y]


static func array_to_vector2i(array : Array) -> Vector2i:
	return Vector2i(array[0], array[1])


static func get_resolution_index(what : Array, dict : Dictionary) -> int:
	var index := 0
	for key in dict.keys():
		if (what[0] == dict[key][0]) and what[1] == dict[key][1] : return index
		index += 1
	return -1
