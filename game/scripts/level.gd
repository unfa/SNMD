extends Node3D
class_name Level

signal level_complete

var level_sha256 : String

var personal_best_time_ms : int = 0

var timer_active : bool = false

var level_elapsed_time_ms : int = 0
var start_time_ms : int = 0
var checkpoint_times_ms : Dictionary = {}
var finish_time_ms : int = 0


func _on_line_activated(time_ms: int, type : TimerTrigger.TimerTriggerType, checkpoint_index : int = 0):
#	print("Game: line activated time_ms: ", time_ms, "; type: ", type,"; checkpoint_index: ", checkpoint_index)
	match type:
		TimerTrigger.TimerTriggerType.START:
			if start_time_ms == 0:
				start_time_ms = time_ms
				timer_active = true
			else:
				return ERR_ALREADY_EXISTS
		TimerTrigger.TimerTriggerType.FINISH:
			if finish_time_ms == 0:
				finish_time_ms = time_ms
				timer_active = false
				level_elapsed_time_ms = finish_time_ms - start_time_ms
				level_complete.emit()
				UserData.add_level_time(level_sha256, level_elapsed_time_ms )
				update_hud(true)
			else:
				return ERR_ALREADY_EXISTS
		TimerTrigger.TimerTriggerType.CHECKPOINT:
			if not checkpoint_times_ms.keys().has(checkpoint_index):
				checkpoint_times_ms[checkpoint_index] = time_ms
			else:
				return ERR_ALREADY_EXISTS


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	var timing_lines : Array[Node] = %TimerTriggers.get_children()
	for line in timing_lines:
		line.on_activated.connect(_on_line_activated)
#		print("Connected signal to timing line ", line)

	personal_best_time_ms = UserData.get_level_time(level_sha256)
	update_hud(true)

func update_hud(update_personal_best := false) -> void:
#	var time_ms : int = level_elapsed_time_ms
#	var mins : int = level_elapsed_time_ms % 60000
#	var secs : int = (level_elapsed_time_ms - mins * 60000) % 6000
#	var ms : int = (level_elapsed_time_ms - mins * 60000 - secs * 6000)
#	var display_string : String = "%02d:%02d.%03d" % [mins, secs, ms];
	if level_elapsed_time_ms > 0:
		%ElapsedTime.text = "elapsed time: %03.3f seconds" % [level_elapsed_time_ms as float / 1000]
	else:
		%ElapsedTime.text = "cross the start to begin"

	if update_personal_best:
		if personal_best_time_ms > 0:
			var prefix = ""
			if level_elapsed_time_ms > 0 and personal_best_time_ms > level_elapsed_time_ms:
				prefix = "previous "
			%PersonalBestTime.text = "%spersonal best time: %03.3f seconds" %\
			[prefix, personal_best_time_ms as float / 1000]
		else:
			%PersonalBestTime.text = "no personal best time on record"


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	if not timer_active:
		return

	level_elapsed_time_ms = Time.get_ticks_msec() - start_time_ms
	update_hud()
