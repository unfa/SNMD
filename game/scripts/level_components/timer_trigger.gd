@tool
class_name TimerTrigger
extends Area3D

enum TimerTriggerType {START, CHECKPOINT, FINISH}

signal on_activated(time_ms : int, type : TimerTrigger.TimerTriggerType, checkpoint_index : int)

var sounds_activated = {
		TimerTriggerType.START : preload("res://assets/sfx/timer_trigger_start.wav"),
		TimerTriggerType.CHECKPOINT : preload("res://assets/sfx/timer_trigger_checkpoint.wav"),
		TimerTriggerType.FINISH : preload("res://assets/sfx/timer_trigger_finish.wav"),
	}
var sound_locked = preload("res://assets/sfx/timer_trigger_locked.wav")

@export var trigger_type := TimerTriggerType.START:
	set(value):
		trigger_type = value
		update_colors()
		update_label()

@export var checkpoint_index : int = 0:
	set(value):
		checkpoint_index = value
		update_label()

@onready var player = get_tree().get_nodes_in_group(&"player")[0]

@onready var light : OmniLight3D = $Light
@onready var decal : Decal = $Decal
@onready var mesh : MeshInstance3D = $Mesh
#@onready var sound : AudioStreamPlayer3D

# these can only be activated once
var activated : bool = false


func update_label():
	if not is_instance_valid($Label):
		return

	$Label.text = str(TimerTriggerType.keys()[trigger_type])
	if trigger_type == TimerTriggerType.CHECKPOINT:
		$Label.text += '\n#' + str(checkpoint_index)


func update_colors() -> void:
	var light_color : Color
	var decal_color : Color
	var mesh_color : Color

	match trigger_type:
		TimerTriggerType.START:
			light_color = Color("abffa6")
			decal_color = Color("00ff00")
			mesh_color = Color("00ff00")

		TimerTriggerType.CHECKPOINT:
			light_color = Color("bbbbbb")
			decal_color = Color("bbbbbb")
			mesh_color = Color("bbbbbb")

		TimerTriggerType.FINISH:
			light_color = Color("ffa6a6")
			decal_color = Color("ff0000")
			mesh_color = Color("ff0000")

	if is_instance_valid(light):
		light.light_color = light_color
	if is_instance_valid(decal):
		decal.modulate = decal_color
	if is_instance_valid(mesh):
		mesh.set_instance_shader_parameter(&"color", mesh_color)


func _ready() -> void:
	if not Engine.is_editor_hint():
		$Label.hide()
		$Label.queue_free()

	await get_tree().process_frame

	update_colors()


func _on_body_entered(body: Node3D) -> void:
	if Engine.is_editor_hint():
		return

	if activated:
		return

	if body.is_in_group(&"player"):
		$Sound.stream = sounds_activated[trigger_type]
		$Sound.play()
		$AnimationPlayer.play("Activated")

		activated = true
		on_activated.emit(Time.get_ticks_msec(), trigger_type, checkpoint_index)
