extends Node

signal load_complete

var AS = AudioServer
const save_file_path = "user://SNMDSettings.json"

var settings : Dictionary = {}

var settings_defaults : Dictionary = {
	"window_mode" = 3,
	"resolution" = [-1, -1],
	"scaling_mode" = 0,
	"resolution_scale" = 1.0,
	"overall_graphics" = "Medium",
	"master_volume" = 1.0,
	"sfx_volume" = 0.5,
	"music_volume" = 0.5,
}


func _enter_tree() -> void:
	load_settings()


func _ready() -> void:
	get_viewport().size_changed.connect(sync_display_graphics_settings)


func save_settings():
	cleanup_wrong_settings()
	var file := FileAccess.open(save_file_path, FileAccess.WRITE_READ)
	var settings_str := JSON.stringify(settings)
	file.store_string(settings_str)
	sync()


func load_settings():
	var file := FileAccess.open(save_file_path, FileAccess.READ)
	if FileAccess.get_open_error() != OK :
		reset_default_settings()
		return

	var settings_temp = JSON.parse_string(file.get_as_text())
	if settings_temp == null :
		reset_default_settings()
		return

	settings = JSON.parse_string(file.get_as_text())

	if !settings.has_all(settings_defaults.keys()) :
		for key in settings_defaults.keys():
			if settings.has(key) : continue
			settings[key] = settings_defaults[key]

	cleanup_wrong_settings()
	sync()


func cleanup_wrong_settings():
	var invalid_settings := []
	for key in settings.keys():
		if !settings_defaults.has(key):
			invalid_settings.append(key)

	for setting in invalid_settings:
		settings.erase(setting)


func sync():
	sync_audio_settings()
	sync_display_graphics_settings()


func sync_audio_settings():
	AS.set_bus_volume_db(AS.get_bus_index(&"Master"), linear_to_db(clamp(settings["master_volume"], 0, 1)))
	AS.set_bus_volume_db(AS.get_bus_index(&"SFX"), linear_to_db(clamp(settings["sfx_volume"], 0, 1)))
	AS.set_bus_volume_db(AS.get_bus_index(&"Music"), linear_to_db(clamp(settings["music_volume"], 0, 1)))


func sync_display_graphics_settings():
	if DisplayServer.window_get_mode() != Window.MODE_MAXIMIZED or settings["window_mode"] != Window.MODE_WINDOWED:
		DisplayServer.window_set_mode(settings["window_mode"])

	var res : Vector2i = CommonMethods.array_to_vector2i(settings["resolution"])
	if res == Vector2i(-1,-1):
		get_window().content_scale_size = Vector2i(maxi(get_viewport().size.x, 1000), maxi(get_viewport().size.y, 500))
	else:
		get_window().content_scale_size = res

	get_viewport().scaling_3d_mode = clamp(settings["scaling_mode"], 0, 1)
	get_viewport().scaling_3d_scale = clamp(settings["resolution_scale"], 0.25, 2)
	if !CommonData.OverallGraphics.has(settings["overall_graphics"]):
		settings["overall_graphics"] = settings_defaults["overall_graphics"]
		print("Faulty graphics settings")
		save_settings()
	#TODO: sync overall_graphics


func reset_default_settings():
	settings = settings_defaults.duplicate(true)
