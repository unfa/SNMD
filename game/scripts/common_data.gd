extends Node
class_name CommonData

static var OverallGraphics := {
	"Potato"    = 0,
	"Low"       = 1,
	"Medium"    = 2,
	"High"      = 3,
	"Very High" = 4,
	"Real Life" = 5
}

static var Resolutions := { #Not using Vector2i because of JSON String formatting
	"Auto"      = [-1,-1],
	"3840x2160" = [3840, 2160],
	"2560x1440" = [2560, 1440],
	"1920x1080" = [1920, 1080],
	"1600x900"  = [1600, 900],
	"1280x720"  = [1280, 720],
	"1024x768"  = [1024, 768],
	"800x600"   = [800, 600],
}

static var ScalingModes := {
	"Bilinear" = 0,
	"FSR"      = 1
}

static var WindowModes := {
	"Fullscreen" = 4,
	"Borderless Fullscreen" = 3,
	"Windowed"   = 0,
}
