extends Node

var dir_path = "user://user_data/"
var level_times_path = dir_path.path_join("level_times")
var dir : DirAccess
var err : int

var level_times : Dictionary = {} # level_sha256 : time_ms


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	dir = DirAccess.open("user://")
	dir.make_dir_recursive(dir_path)
	if dir.dir_exists(dir_path):
		dir = DirAccess.open(dir_path)
		if dir.file_exists(level_times_path):
			load_level_times()
	else:
		dir.make_dir_recursive(dir_path)


func load_level_times() -> void:
	var file = FileAccess.open(level_times_path, FileAccess.READ)
	level_times = str_to_var(file.get_as_text())
	file.close()


func save_level_times() -> void:
	var file = FileAccess.open(level_times_path, FileAccess.WRITE)
	file.store_string(var_to_str(level_times))
	file.close()


func get_level_time(level_sha256):
	if level_times.keys().has(level_sha256):
		return level_times[level_sha256]
	else:
		return 0


func add_level_time(level_sha256, time_ms):
	print("Processing time record of ", (time_ms as float / 1000), " seconds for level ", level_sha256, " ...")
	if not level_times.keys().has(level_sha256):
		level_times[level_sha256] = time_ms
		print("Added personal best time")
		save_level_times()
	elif level_times[level_sha256] > time_ms:
		level_times[level_sha256] = time_ms
		print("Updated personal best time")
		save_level_times()
	else:
		print("Personal best not updated")
