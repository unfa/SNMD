extends DirectionalLight3D

@onready var halo : Sprite3D = $Halo
@onready var halo_check : Node3D = $ShapeCast3D
@onready var halo_max_px_size : float = halo.pixel_size

var halo_check_count : int = 3
var halo_check_radius : float = 16


func _ready() -> void:
	halo.show()


func _process(delta: float) -> void:
	var cam = get_viewport().get_camera_3d()
	halo.global_position = cam.global_position + self.global_transform.basis.z * 400
	var cols : int = 0

	seed(1)

	for i in range(0,halo_check_count):
		var offset = Vector3(randf_range(-1,1), randf_range(-1,1), randf_range(-1,1)).normalized() * halo_check_radius
		halo_check.global_position = halo.global_position + offset
		halo_check.look_at(cam.global_position)
		var distance = halo_check.global_position.distance_to(cam.global_position)
		halo_check.target_position = Vector3.FORWARD * (distance * 4)
		halo_check.force_shapecast_update()

		if halo_check.is_colliding():
			var collider = halo_check.get_collider(0)
			if  collider is Area3D and collider.get_parent() == cam:# HaloDetection
				cols += 1

	var coverage = clamp(cols as float / (halo_check_count as float) * 2 - 1, 0, 1)
	var px_size = lerp(.0, halo_max_px_size, coverage)

	if cols == 0:
		halo.hide()
	else:
		halo.show()

	halo.pixel_size = lerpf(halo.pixel_size, px_size, pow(clamp(-30 * delta,-1, 0), 2))
