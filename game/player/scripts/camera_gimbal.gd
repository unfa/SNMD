extends Node3D
##Player node.
@export var player: Player
##Yaw node.
@export var cameraYaw: Node3D
##Pitch node.
@export var cameraPitch: Node3D
##Tilt node.
@export var cameratilt: Node3D
##springArm node.
@export var springArm: SpringArm3D
##Camera node.
@export var camera: Camera3D
##Mouse sensibility.
@export var mouseSensibility: float = 0.01
##Controller sensibility.
@export var controllerSensibility: float = 0.1
##Max Pitch.
@export var maxPitch: float = 75
##Min Pitch.
@export var minPitch: float = -75
##min camera distance.
@export var minDistance: float = 5
##max camera distance.
@export var maxDistance: float = 15
##zoom amount distance.
@export var zoomAmount: float = 0.5
##Non linear camera look speed.
@export var lookSpeed: float = 10
##Amount of camera look prediction based on the player velocity.
@export var lookPrediction: float = 2.5
##Non linear speed to interpolate the camera gimbal with the surface.
@export var cameraAlignSpeed: float = 30

var mouseInput: Vector2
var controllerInput: Vector2

func _ready():
	springArm.spring_length = (minDistance + maxDistance) / 2


func _input(event):
	if Input.is_action_pressed("camera_zoom_in"):
		springArm.spring_length -= zoomAmount
		springArm.spring_length = clamp(springArm.spring_length, minDistance, maxDistance)
	if Input.is_action_pressed("camera_zoom_out"):
		springArm.spring_length += zoomAmount
		springArm.spring_length = clamp(springArm.spring_length, minDistance, maxDistance)

	if event is InputEventMouseMotion:
		#Event scaled by magic number because godot editor does not allow
		#more than 3 decimals. Also, in godot this is resolution dependent.
		#if we want constant sensibility we have to implement it.
		mouseInput = event.relative * 0.01 


func _process(delta):
	
	var newYawBasis: Basis = cameraYaw.global_transform.basis
	
	if player.collision_check().collision_normal != Vector3.ZERO:
		newYawBasis.y = player.collision_check().collision_normal
		newYawBasis.x = -newYawBasis.z.cross(player.collision_check().collision_normal).normalized()
	else:
		if !player.apply_snap:
			newYawBasis.y = Vector3.UP
			newYawBasis.x = -newYawBasis.z.cross(Vector3.UP).normalized()
	
	newYawBasis = newYawBasis.orthonormalized()
	
	var yawAlignSpeed = clamp(pow(-cameraAlignSpeed * delta, 2), 0, 1 ) 
	cameraYaw.global_transform.basis = cameraYaw.global_transform.basis.slerp(newYawBasis, yawAlignSpeed).orthonormalized()
	
	#GIMBAL CONTROL
	
	#Controller input
	controllerInput = Input.get_vector("look_left", "look_right", "look_up", "look_down")
	controllerInput = controllerInput * controllerSensibility * delta
	
	#Add mouse and controller input.
	var finalInput: Vector2 = controllerInput + mouseInput * mouseSensibility
	
	#Rotate the gimbal.
	cameraYaw.rotate(cameraYaw.basis.y, -finalInput.x )
	cameraPitch.rotate(cameraPitch.basis.x, -finalInput.y)
	cameraPitch.rotation.x = clamp(cameraPitch.rotation.x, deg_to_rad(minPitch) , deg_to_rad(maxPitch))

	#orthonormalize.
	self.orthonormalize()
	cameraYaw.orthonormalize()
	cameraPitch.orthonormalize()
	cameratilt.orthonormalize()

	#Reset mouse input to zero.
	mouseInput = Vector2.ZERO

	#CAMERA CONTROL

	#look At
	var lookAtPos: Vector3 = player.global_position + player.linear_velocity * delta * lookPrediction
	var lookAtBasis = camera.global_transform.looking_at(lookAtPos, cameraYaw.global_transform.basis.y).basis
	var lookAtSpeed = clamp(pow(-lookSpeed * delta, 2), 0, 1 ) 

	camera.global_transform.basis = camera.global_transform.basis.slerp(lookAtBasis, lookAtSpeed)
	camera.orthonormalize()

func _physics_process(_delta):
	self.global_position = player.global_position
