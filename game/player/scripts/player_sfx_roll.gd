extends Node3D

@export var player : Player

@onready var layers : Array[Node] = get_children()
#@export var layers : Array[Node] #= get_children()

@export var debug_panel : Control

@export var max_velocity : float = 600
@export var full_volume_vel : float = 150

@export var use_debug_panel : bool = true:
	set(value):
		use_debug_panel = value
		if is_instance_valid(debug_panel):
			debug_panel.visible = use_debug_panel

var top_layer : int
var bottom_layer : int
var prev_top_layer : int
var prev_bottom_layer : int


func _ready() -> void:
	for i in layers:
		i.volume_db = -INF

	if is_instance_valid(debug_panel):
		debug_panel.visible = use_debug_panel


func _physics_process(delta: float) -> void:

	if not player.apply_snap:
		for i in layers:
			i.volume_db = -INF
		return

	var player_vel = player.linear_velocity.length()

	var layer = clamp(remap(player_vel, 0, max_velocity, 0, 1), 0, 1) * (layers.size() -1)

	var base_volume = linear_to_db(clamp(remap(player.linear_velocity.length(), 1, full_volume_vel, 0, 1), 0, 1))

	top_layer = clampi(ceili(layer), 0, layers.size())
	bottom_layer = clampi(floori(layer), 0, layers.size() - 1)

	layers[top_layer].volume_db = linear_to_db((1.0 - abs(top_layer as float - layer) ** 2) * db_to_linear(base_volume))
	layers[bottom_layer].volume_db = linear_to_db((1.0 - abs(layer - bottom_layer as float) ** 2) * db_to_linear(base_volume))

	# silence layers out of active range
	if top_layer < prev_top_layer:
		for i in range(top_layer, layers.size()):
			layers[i].volume_db = -INF
	elif bottom_layer > prev_bottom_layer:
		for i in range(0, bottom_layer):
			layers[i].volume_db = -INF

	prev_top_layer = top_layer
	prev_bottom_layer = bottom_layer

	if use_debug_panel:
		debug_panel.get_node("HBoxContainer/ProgressBar").value = layer
		printt("player_vel:", round(player_vel), "layer:", (round(layer * 100) as float) / 100, "round(layer):",  round(layer))# "xfade:", xfade) # "Roll 01 volume_db:", $Roll01.volume_db,
		for i in range(layers.size()):
			var mod_color : Color
			if i == bottom_layer && i == top_layer: mod_color = Color.YELLOW
			elif i == bottom_layer:  mod_color = Color.RED
			elif i == top_layer:  mod_color = Color.GREEN
			elif i < bottom_layer: mod_color = Color.DARK_RED
			elif i > top_layer:  mod_color = Color.DARK_GREEN
			else: mod_color = Color.DIM_GRAY

			mod_color.a = db_to_linear(layers[i].volume_db)
			debug_panel.get_node("HBoxContainer/VBoxContainer/" + layers[i].name).modulate = mod_color
