extends Node3D

@export var player : RigidBody3D
@export var spin_range : Vector2
@export var speed_range : Vector2
@export var spin_pitch_curve : Curve
@export var pitch_range : Vector2
@export var spin_volume_curve : Curve
@export var noise_volume_curve : Curve
@export var noise_highpass_curve : Curve
@export var highpass_range : Vector2
@export var noise_lowpass_curve : Curve
@export var lowpass_range : Vector2
@export var wind_lowpass_curve : Curve
@export var wind_volume_curve : Curve
@export var wind_lowpass_range : Vector2
@export var impact_volume_range : Vector2
@export var impact_audio_clips : Array[AudioStream]
@export var skid_range : Vector2
@export var skid_audio_clips : Array[AudioStream]

@onready var bus : StringName = $Noise.bus
@onready var bus_idx : int = AudioServer.get_bus_index(bus)
@onready var hp_filter : AudioEffectHighPassFilter = AudioServer.get_bus_effect(bus_idx, 0) # hardcoded index - FIXME
@onready var lp_filter : AudioEffectLowPassFilter = AudioServer.get_bus_effect(bus_idx, 1) # hardcoded index - FIXME

@onready var wind_bus : StringName = $Wind.bus
@onready var wind_bus_idx : int = AudioServer.get_bus_index(wind_bus)
@onready var wind_lp_filter : AudioEffectLowPassFilter = AudioServer.get_bus_effect(wind_bus_idx, 0) # hardcoded index - FIXME


func _ready() -> void:
	player.connect(&"on_impact", on_impact)


func on_impact(impact_force):
	var sfx_idx : int = floori(remap(impact_force, 0, 1, 0, impact_audio_clips.size() - 1))
	$Impact.stream = impact_audio_clips[sfx_idx]

	# FIXME volume is always -inf
#	$Impact.volume_db = linear_to_db(remap(impulse_power, 0, 1, impact_volume_range.x, impact_volume_range.y))
#	printt("volume", $Impact.volume_db)
	$Impact.play()

	$"../AnimationPlayer".play("Impact")
	$"../AnimationPlayer".seek(1 - impact_force)


func _process(delta: float) -> void:
	pass


func _physics_process(delta: float) -> void:
	var spin = remap(player.angular_velocity.length(), spin_range.x, spin_range.y, 0, 1)

	$Spin.pitch_scale = remap(spin_pitch_curve.sample_baked(spin), 0, 1, pitch_range.x, pitch_range.y)
#	$Spin.volume_db = linear_to_db(spin_volume_curve.sample_baked(spin) * player.ball_throttle)
	$Spin.volume_db = linear_to_db(player.ball_throttle)

	$Noise.volume_db = linear_to_db(noise_volume_curve.sample_baked(spin))

	# controlling audio effects applied to the Spin sound on a mixer bus
	var speed = remap(player.linear_velocity.length(), speed_range.x, speed_range.y, 0, 1)
	hp_filter.cutoff_hz = remap(noise_highpass_curve.sample_baked(speed), 0, 1, highpass_range.x, highpass_range.y)
	lp_filter.cutoff_hz = remap(noise_lowpass_curve.sample_baked(speed), 0, 1, lowpass_range.x, lowpass_range.y)

	$Wind.volume_db = linear_to_db(wind_volume_curve.sample_baked(speed))
	wind_lp_filter.cutoff_hz = remap(wind_lowpass_curve.sample_baked(speed), 0, 1, wind_lowpass_range.x, wind_lowpass_range.y)
