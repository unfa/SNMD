class_name Player
extends RigidBody3D

@export_category("Nodes")
@export var camera: Camera3D

@export_category("Settings")
@export var ACCELERATION: float = 50000
@export var AIR_ACCELERATION: float = 12500
@export var JUMP_SPEED: float = 20
@export var SNAP_FORCE: float = 2500
@export var SNAP_DISTANCE: float = 10
@export_category("Debug")
@export var input_basis_debug: Node3D
@export var aligned_wish_dir_debug: Node3D
@export var show_debug: bool = false

@onready var normal_angular_damp := self.angular_damp
@export var brake_angular_damp : float = 100
@onready var normal_linear_damp := self.linear_damp
@export var brake_linear_damp : float = 0.25
@export var impact_force_range := Vector2(1000, 50000) # recognized minimum to maximum

var apply_snap: bool = false
var holding_brake: bool = false

var last_contact_normal: Vector3 = Vector3.ZERO
@export var impact_normal_max_dot := 0.99 # used to check if a contact was an impact

var ball_throttle : float = 0.0

signal on_impact(impact_force : float)

enum BALL_STATE {IDLE, ACCELERATE, JUMP, IMPACT, BRAKE}

@export var ball_state_light_colors : Dictionary = {
	BALL_STATE.IDLE : Color("c5fff9"),
	BALL_STATE.ACCELERATE : Color("39e2ff"),
	BALL_STATE.JUMP : Color("87f9ff"),
	BALL_STATE.IMPACT : Color("1852ff"),
	BALL_STATE.BRAKE : Color("ff0c06")
}

var ball_state := BALL_STATE.IDLE:
	set(value):
		if ball_state_light_colors.keys().has(value):
			$OmniLight3D.light_color = ball_state_light_colors[value]
		if ball_state == BALL_STATE.BRAKE:
			var speed = linear_velocity.length_squared() + angular_velocity.length_squared()
#			printt("speed:", round(speed), "lin vel len sqr", round(linear_velocity.length_squared()), "ang vel len sqr ", round(angular_velocity.length_squared()))
			if ball_state != value:
				$SFX/Brake.volume_db = -80 # start quieter to avoid a bad click
			create_tween().tween_property($SFX/Brake, "volume_db", remap(speed, 0, 50000, -26, 12), 0.075)
#			$SFX/Brake.pitch_scale = remap(speed, 0, 300, -0.48, -0.3)
			$SFX/Brake.play(0.02)

		ball_state = value


func _ready() -> void:
	$TestFloor.hide()
	$TestFloor.queue_free()
	Input.mouse_mode = Input.MOUSE_MODE_CAPTURED


func _input(event: InputEvent) -> void:
	if not event.is_action("move_brake"):
		return

	if event.is_pressed():
		holding_brake = true
		self.angular_damp = brake_angular_damp
		self.linear_damp = brake_linear_damp
	if event.is_released():
		holding_brake = false
		self.angular_damp = normal_angular_damp
		self.linear_damp = normal_linear_damp


func _measure_impact_force(data) -> float:
		#print("Impact sfx! data: ", data)
	var impact = data.collision_impulse.length()
	var normal = data.collision_normal
	var last_normal = data.last_contact_normal
	var normal_dot = normal.dot(last_normal)

	if impact < 1000:
		return 0

#	printt("normal dot", normal_dot, "snap", data.apply_snap)

	if normal_dot > impact_normal_max_dot and data.apply_snap:
		return 0

	var impact_force: float = clamp(remap(impact, impact_force_range.x, impact_force_range.y, 0, 1), 0, 1)

	return impact_force


func _integrate_forces(state):
	# jump
	var col_check = collision_check()

	if col_check.is_colliding:
		last_contact_normal = col_check.collision_normal
		apply_snap = true
		var force = _measure_impact_force(col_check)
		if force > 0:
			on_impact.emit(force)

	state.apply_central_force(Vector3.ZERO)

	if Input.is_action_just_pressed("move_jump") and apply_snap:
		apply_snap = false
		state.apply_central_impulse(col_check.collision_normal * JUMP_SPEED * mass)
		$AnimationPlayer.play(&"Jump")
		ball_state = BALL_STATE.JUMP

	if apply_snap:
		if test_move(self.global_transform, -last_contact_normal * SNAP_DISTANCE):
			state.apply_central_force(-last_contact_normal * SNAP_FORCE * mass)
		else:
			apply_snap = false

	%SnapDecal.visible = apply_snap

	accelerate()


# wish direction in global space.
func get_wish_direction()-> Vector3:

	var input_vector: Vector2
	var wish_direction: Vector3

	input_vector = Input.get_vector("move_left", "move_right", "move_backwards", "move_forward")
	wish_direction = Vector3(input_vector.x, 0, -input_vector.y)

	return wish_direction


#Check for collision data and returns a dictionary with the results.
func collision_check()-> Dictionary:
	var state: PhysicsDirectBodyState3D = PhysicsServer3D.body_get_direct_state(self.get_rid())
	var results: Dictionary = {
		"is_colliding": false,
		"collision_normal": Vector3.ZERO,
		"collision_impulse" : Vector3.ZERO,
		"last_contact_normal" : Vector3.ZERO,
		"apply_snap" : false,
		"holding_brake" : holding_brake,
		}

	results.last_contact_normal = last_contact_normal
	results.apply_snap = apply_snap

	if state.get_contact_count() == 0:
		return results

	results.is_colliding = true

	for i in state.get_contact_count():
		results.collision_normal += state.get_contact_local_normal(i)
		results.collision_impulse += state.get_contact_impulse(i)

	results.collision_normal = results.collision_normal.normalized()

	return results


func accelerate()->void:
	var state: PhysicsDirectBodyState3D = PhysicsServer3D.body_get_direct_state(self.get_rid())
	var input_basis: Basis

	#BASIS HELPER

	#Start from the camera basis.
	input_basis = camera.global_transform.basis

	if collision_check().is_colliding:
		#Align with the surface we are touching.
		input_basis.y = collision_check().collision_normal
		input_basis.x = -input_basis.z.cross(collision_check().collision_normal).normalized()
		input_basis = input_basis.orthonormalized()

		#Project the collision normal into the X Y plane.
		var horizontal_collision_normal: Vector3
		horizontal_collision_normal = Plane(input_basis.z).project(collision_check().collision_normal)

		#If the result of the dot product with the camera Y axis
		#is under 0.1 (0 plus a margin), we invert the x component
		#to keep the horizontal movement correct respect to the camara.
		if horizontal_collision_normal.dot(camera.global_transform.basis.y) < -0.1:
			input_basis.x = -input_basis.x

	#WISH DIRECTION
	var wish_dir: Vector3 = get_wish_direction()
	var alignes_wish_dir: Vector3 = Vector3()

	if holding_brake:
		ball_state = BALL_STATE.BRAKE
		return
	elif wish_dir.length() == 0:
		ball_state = BALL_STATE.IDLE
	else:
		ball_state = BALL_STATE.ACCELERATE
#		ball_throttle = wish_dir.length()

	#Reproject the "standard" world space wish direction
	#into out new basis to align it to the surface.
	alignes_wish_dir += input_basis.x * wish_dir.x
	alignes_wish_dir += input_basis.y * wish_dir.y
	alignes_wish_dir += input_basis.z * wish_dir.z
	alignes_wish_dir = alignes_wish_dir.normalized()

	#ACCELERATION

	#The rotation axis to rotate towards a direction
	#is the surface normal cross the the wish direction (Normalized)
	var rotation_axis: Vector3

#	if holding_brake:
#		return

	if collision_check().is_colliding:
		rotation_axis = collision_check().collision_normal.cross(alignes_wish_dir).normalized()
		#Apply the torque
		state.apply_torque(rotation_axis * ACCELERATION)
		ball_throttle = clampf(ACCELERATION as float * alignes_wish_dir.length() as float / 10000.0 , 0 ,1)
	else:
		rotation_axis = camera.global_transform.basis.y.cross(alignes_wish_dir).normalized()
		#Apply the torque
#		print("Air accel: ", rotation_axis * AIR_ACCELERATION)
		state.apply_torque(rotation_axis * AIR_ACCELERATION)
		ball_throttle = clampf(AIR_ACCELERATION as float * alignes_wish_dir.length() as float / 10000.0 , 0 ,1)

	if show_debug:
		input_basis_debug.visible = true
		aligned_wish_dir_debug.visible = true
		aligned_wish_dir_debug.global_position = self.global_position
		input_basis_debug.global_position = self.global_position

		#Input basis
		input_basis_debug.global_transform.basis = input_basis

		#wish direction
		if aligned_wish_dir_debug.global_position != aligned_wish_dir_debug.global_position + alignes_wish_dir:
			aligned_wish_dir_debug.visible = true
			aligned_wish_dir_debug.look_at(aligned_wish_dir_debug.global_position + alignes_wish_dir)
		else:
			aligned_wish_dir_debug.visible = false

	else:
		input_basis_debug.visible = false
		aligned_wish_dir_debug.visible = false


#SnapByContactPoint
#func snap()->void:
#
#	var state: PhysicsDirectBodyState3D = PhysicsServer3D.body_get_direct_state(self.get_rid())
#
#	for i in state.get_contact_count():
#		state.apply_central_force(-state.get_contact_local_normal(i) * SNAP_FORCE)


#SnapByNormal
func snap()->void:
	var state: PhysicsDirectBodyState3D = PhysicsServer3D.body_get_direct_state(self.get_rid())

	if collision_check().is_colliding:
		state.apply_central_force(-collision_check().collision_normal * SNAP_FORCE)



