extends Resource
class_name LevelMetadata

@export var level_name : String
@export var level_hidden : bool = false
#var level_subdirectory : String
#@export var level_group : String
#@export var level_sorting_index : int = 0 # what should be used
#@export var level_locked : bool = false
#@export var level_author : String = "SNMD developers"
#var level_scene_path : String = "SCENE PATH"
#var level_image_path : String = "IMAGE PATH"
#@export var level_preview_image : ImageTexture
