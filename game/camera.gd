extends Node3D

##Player node.
@export var player: RigidBody3D

##Camera node.
@export var camera: Node3D

##The difference in position between the current and the target position
##will be rised to this value to compute the velocity.
@export var exponent: float = 2.0

##Min camera speed
@export var minSpeed: float = 1.0

##Max camera distance
@export var maxDistance: float = 10.0

##Non linear camera look speed.
@export var lookSpeed: float = 20.0

##Amount of camera look prediction based on the player velocity.
@export var lookPrediction: float = 5.0

func _process(delta):
	#look At
	var lookAtPos: Vector3 = player.global_position + player.linear_velocity * delta * lookPrediction
	var lookAtBasis = camera.global_transform.looking_at(lookAtPos, Vector3.UP).basis
	var slerpSpeed = clamp(pow(-lookSpeed * delta, 2), 0, 1 ) 

	camera.global_transform.basis = camera.global_transform.basis.slerp(lookAtBasis, slerpSpeed)
	camera.orthonormalize()

func _physics_process(delta):
	var currentPos: Vector3 = self.global_position
	var targetPos: Vector3 = player.global_position
	var posDistance: float = currentPos.distance_to(targetPos)
	var finalSpeed : float = clamp(pow(posDistance, exponent), minSpeed, INF) 
	var newPos : Vector3 = currentPos.move_toward(targetPos, finalSpeed * delta)

	if newPos.distance_to(targetPos) < maxDistance:
		self.global_position = newPos
		return

	var currentDirection: Vector3 = targetPos.direction_to(currentPos)
	self.global_position = targetPos + currentDirection * maxDistance

