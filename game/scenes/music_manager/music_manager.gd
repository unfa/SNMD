extends Node

@onready var _player = $MusicPlayer


func _ready() -> void:
	_player.playing = true
	_player.stream_paused = true


func play() -> void:
	_player.stream_paused = false


func stop() -> void:
	_player.stream_paused = true
