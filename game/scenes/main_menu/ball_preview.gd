extends AspectRatioContainer

@export var rotation_speed : float = 1

@onready var ball_mesh: MeshInstance3D = $BallPreviewViewport/SubViewport/Node3D/BallMesh
@onready var sub_viewport: SubViewport = $BallPreviewViewport/SubViewport
@onready var viewport: SubViewportContainer = $BallPreviewViewport




# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	ball_mesh.rotation.y += delta
	
	sub_viewport.scaling_3d_mode = get_viewport().scaling_3d_mode
	sub_viewport.scaling_3d_scale = get_viewport().scaling_3d_scale




