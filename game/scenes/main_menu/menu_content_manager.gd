extends SubViewportContainer
class_name MenuContentManager

signal content_changed()

@export var content_transition_type : Tween.TransitionType = Tween.TRANS_CUBIC
@export var transition_duration : float = 1

@onready var play_menu_content: HBoxContainer = %PlayMenuContent
@onready var settings_menu_content: HBoxContainer = %SettingsMenuContent
@onready var map_select_grid: GridContainer = play_menu_content.get_node("PlayMenuContainer/LevelSelectGrid")
@onready var sub_viewport: SubViewport = $SubViewport
@onready var menu_content_container: MarginContainer = $SubViewport/MenuContentScroll/MenuContentContainer
@onready var menu_content_scroll: ScrollContainer = $SubViewport/MenuContentScroll


enum States {PLAY, SETTINGS}

var selected : States = States.PLAY :
	set(new_selected):
		if selected == new_selected : return
		switch_nodes_visibility(node_state(selected), node_state(new_selected))
		selected = new_selected
		content_changed.emit()


var snv_tw : Tween # short for switch_node_visibility_tween

func _ready() -> void:
	startup_fade_in()
	_connect_signals()
	resize()


func _connect_signals():
	get_viewport().size_changed.connect(resize)


func node_state(state : States) -> Control:

	match(state):
		States.PLAY: return play_menu_content
		States.SETTINGS: return settings_menu_content
		_: return null


func switch_nodes_visibility(from : Control, to : Control):
	if !from or !to : return
	if snv_tw != null : snv_tw.kill()
	snv_tw = create_tween().set_trans(content_transition_type)

	to.modulate = Color(1,1,1,0)
	to.visible = true

	snv_tw.tween_property(self, "modulate", modulate * Color(1, 1, 1, 0), transition_duration/2)
	snv_tw.tween_callback(func() : from.visible = false)
	snv_tw.tween_property(self, "modulate", modulate + Color(0, 0, 0, 1), transition_duration/2)
	snv_tw.parallel().tween_property(to, "modulate", Color(1, 1, 1, 1), transition_duration/2)


func startup_fade_in():
	for content in menu_content_container.get_children():
		content.visible = false

	modulate.a = 0
	play_menu_content.visible = true

	if snv_tw != null : snv_tw.kill()
	snv_tw = create_tween().set_trans(content_transition_type)
	snv_tw.tween_property(self, "modulate", modulate + Color(0, 0, 0, 1), transition_duration)


func resize():
	await get_tree().process_frame
	if !map_select_grid : return
	#Check if grid can have more items per row
	var grid_item_size_x : float = map_select_grid.get_child(0).size.x
	map_select_grid.columns = int(size.x / grid_item_size_x)
	menu_content_scroll.custom_minimum_size = size
	visible = false
	visible = true


