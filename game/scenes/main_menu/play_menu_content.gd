extends MenuContent
class_name PlayMenuContent

@export_dir var levels_directory_search_path : String = "res://levels/"
@onready var level_select_tile_scene = preload("res://scenes/ui/ui_level_select_tile.tscn")


func _ready():
	var dir = DirAccess.open(levels_directory_search_path)
	var levels = dir.get_directories()

	for level in levels:
		var metadata = load(levels_directory_search_path.path_join(level).path_join("level_metadata.tres"))
		var scene_path = levels_directory_search_path.path_join(level).path_join("level_scene.tscn")
		var image_path = levels_directory_search_path.path_join(level).path_join("level_preview.png")
		var new_tile : LevelSelectTile = level_select_tile_scene.instantiate()
		new_tile.level_scene_path = scene_path
		new_tile.level_image_path = image_path
		new_tile.level_metadata = metadata
		%LevelSelectGrid.add_child(new_tile)

	self.first_focus_node = %LevelSelectGrid.get_child(0)
