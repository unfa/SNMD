extends CanvasLayer

@export var menu : Node

func show_loading_screen() -> void:
	menu.show_loading_screen()


func hide_loading_screen() -> void:
	menu.hide_loading_screen()


func hide_pause_menu() -> void:
	menu.hide_pause_menu()


func show_pause_menu() -> void:
	menu.show_pause_menu()
