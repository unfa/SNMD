extends MenuContent
class_name SettingsMenuContent


@onready var master_volume_option : OptionSlider = $Settings/Audio/MasterVolume
@onready var sfx_volume_option : OptionSlider = $Settings/Audio/SfxVolume
@onready var music_volume_option : OptionSlider = $Settings/Audio/MusicVolume
@onready var window_mode: OptionList = $Settings/DisplayAndGraphics/WindowMode
@onready var resolution_option: OptionList = $Settings/DisplayAndGraphics/Resolution
@onready var scaling_mode: OptionList = $Settings/DisplayAndGraphics/ScalingMode
@onready var resolution_scale_option: OptionSlider = $Settings/DisplayAndGraphics/ResolutionScale
@onready var overall_graphics_option: OptionList = $Settings/DisplayAndGraphics/OverallGraphics

var AS  = AudioServer

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	_connect_signals()
	sync_settings()


func _connect_signals():
	window_mode.option_button.item_selected.connect(on_option_list_value_changed.bind(window_mode))
	resolution_option.option_button.item_selected.connect(on_option_list_value_changed.bind(resolution_option))
	scaling_mode.option_button.item_selected.connect(on_option_list_value_changed.bind(scaling_mode))
	resolution_scale_option.slider.value_changed.connect(on_option_slider_value_changed.bind(resolution_scale_option))
	overall_graphics_option.option_button.item_selected.connect(on_option_list_value_changed.bind(overall_graphics_option))
	
	master_volume_option.slider.value_changed.connect(on_option_slider_value_changed.bind(master_volume_option))
	sfx_volume_option.slider.value_changed.connect(on_option_slider_value_changed.bind(sfx_volume_option))
	music_volume_option.slider.value_changed.connect(on_option_slider_value_changed.bind(music_volume_option))




func sync_settings():
	window_mode.select_with_text(CommonData.WindowModes.find_key(int(SaveGlobal.settings[window_mode.save_name])))
	resolution_option.option_button.\
		select(CommonMethods.get_resolution_index\
			(SaveGlobal.settings[resolution_option.save_name], CommonData.Resolutions))
			
	scaling_mode.option_button.select(SaveGlobal.settings[scaling_mode.save_name])
	resolution_scale_option.slider.value = SaveGlobal.settings[resolution_scale_option.save_name]
	overall_graphics_option.option_button.select(CommonData.OverallGraphics[SaveGlobal.settings[overall_graphics_option.save_name]])
	
	master_volume_option.slider.value = SaveGlobal.settings[master_volume_option.save_name]
	sfx_volume_option.slider.value = SaveGlobal.settings[sfx_volume_option.save_name]
	music_volume_option.slider.value = SaveGlobal.settings[music_volume_option.save_name]



func on_option_slider_value_changed(value, node : OptionSlider):
	SaveGlobal.settings[node.save_name] = value
	SaveGlobal.save_settings()


func on_option_list_value_changed(value_idx, node : OptionList):
	match node.type:
		node.Types.RESOLUTION:
			SaveGlobal.settings[node.save_name] = \
			CommonData.Resolutions[node.option_button.get_item_text(node.option_button.selected)]
		
		node.Types.SCALING_MODE:
			SaveGlobal.settings[node.save_name] = value_idx
		
		node.Types.WINDOW_MODE:
			SaveGlobal.settings[node.save_name] = CommonData.WindowModes[node.option_button.get_item_text(value_idx)]
		
		_:
			SaveGlobal.settings[node.save_name] = node.option_button.get_item_text(value_idx)
			
			
	SaveGlobal.save_settings()
