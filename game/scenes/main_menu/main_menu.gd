extends Control
class_name MainMenu

enum Types{MAIN_MENU, PAUSE_MENU}

@export var menu_type : Types = Types.MAIN_MENU :
	set(value):
		menu_type = value
		if not self.is_inside_tree():
			await ready
		setup_menu()

@export var blur_mix : float = 0.7
@export var blur_amount : float = 2.2

@onready var resume_button: Button = $MainButtonContainer/ResumeButton
@onready var play_button: Button = $MainButtonContainer/PlayButton
@onready var side_panel_mat: ShaderMaterial = $SidePanel.material
@onready var menu_content_panel_mat: ShaderMaterial = $MenuContentPanel.material
@onready var menu_content_manager: SubViewportContainer = $MenuContentPanel/MenuContentViewport

@export var pause_component : Node


# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	setup_menu()
	if menu_type == Types.MAIN_MENU:
		play_button.grab_focus()
	else:
		resume_button.grab_focus()



func show_loading_screen() -> void:
	%LoadingScreen.show()


func hide_loading_screen() -> void:
	%LoadingScreen.hide()


func hide_pause_menu() -> void:
	release_focus()
	assert(is_instance_valid(pause_component), "can't hide pause menu - pause component is " + str(pause_component))
	pause_component.fade_out()


func show_pause_menu() -> void:
	assert(is_instance_valid(pause_component), "can't show pause menu - pause component is " + str(pause_component))
	pause_component.fade_in()
	resume_button.grab_focus()


func setup_menu():
	side_panel_mat.set_shader_parameter("blur_amount", blur_amount)
	menu_content_panel_mat.set_shader_parameter("blur_amount", blur_amount)
	match menu_type:
		Types.MAIN_MENU:
			resume_button.visible = false
			side_panel_mat.set_shader_parameter("blur_mix", 0)
			menu_content_panel_mat.set_shader_parameter("blur_mix", 0)

		Types.PAUSE_MENU:
			resume_button.visible = true
			assert(is_instance_valid(pause_component), "Paus menu can't locate it's pause menu component!")
#			canvas = get_parent()
#			pause_component.canvas = canvas
			modulate = Color(modulate, 0)
			side_panel_mat.set_shader_parameter("blur_mix", blur_mix)
			menu_content_panel_mat.set_shader_parameter("blur_mix", blur_mix)
			visible = true                  #
			await get_tree().process_frame  #this is a hack to properly pivot buttons in the hoverclickanimcomponent
			visible = false                 #


func on_play_button():
	menu_content_manager.selected = MenuContentManager.States.PLAY


func on_settings_button():
	menu_content_manager.selected = MenuContentManager.States.SETTINGS


func on_quit_button():
	$QuitConfirmationDialog.popup()


func _on_confirmation_dialog_confirmed() -> void:
	get_tree().quit()


func _notification(what: int) -> void:
	match what:
		NOTIFICATION_APPLICATION_FOCUS_OUT:
			Engine.max_fps = 1
#			print("limiting FPS")
		NOTIFICATION_APPLICATION_FOCUS_IN:
			Engine.max_fps = 0
#			print("Removing FPS limit")
