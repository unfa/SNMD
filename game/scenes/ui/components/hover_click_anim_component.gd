extends Node
class_name HoverClickAnimComponent

@export var node_to_animate : Control

@export_group("Specific Overrides")

@export var scale_node : Control : 
	get:
		return scale_node if scale_node != null else node_to_animate

@export var hover_signals_node : Control :
	get:
		return hover_signals_node if hover_signals_node != null else node_to_animate

@export var click_signals_button : BaseButton : 
	get:
		if click_signals_button == null and node_to_animate is BaseButton: 
			click_signals_button = node_to_animate
		return click_signals_button

@export_group("Animation Settings")
@export var hover_scale : Vector2 = Vector2(1.1, 1.1)
@export var hover_anim_duration : float = 0.2
@export var hover_anim_trans_type : Tween.TransitionType = Tween.TRANS_CUBIC
@export var click_anim_trans_type : Tween.TransitionType = Tween.TRANS_QUART

var scale_tw : Tween


enum States {NORMAL,
 HOVER,
 CLICK,
}


var current_state : States = States.NORMAL : 
	set(value):
		current_state = value
		on_state_change()



# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	node_to_animate = get_parent()
	_connect_signals()
	await get_tree().process_frame
	center_pivot()
	node_to_animate.visibility_changed.connect(center_pivot)




func center_pivot():
	scale_node.pivot_offset = scale_node.size / 2.0
	
func _connect_signals():
	hover_signals_node.mouse_entered.connect(on_mouse_entered)
	hover_signals_node.mouse_exited.connect(on_mouse_exited)
	hover_signals_node.focus_entered.connect(on_mouse_entered)
	hover_signals_node.focus_exited.connect(on_mouse_exited)
	
	if click_signals_button: click_signals_button.pressed.connect(on_pressed)


func on_state_change():
	handle_hover()
	handle_click()


func handle_hover():
	match current_state:
		States.NORMAL: hover_anim(scale_node, Vector2(1,1))
		States.HOVER: hover_anim(scale_node, hover_scale)


func handle_click():
	if current_state != States.CLICK : return
	scale_node.scale = ((hover_scale - Vector2(1,1)) * 0.5) + Vector2(1,1)
	current_state = States.HOVER
#	click_anim(map_preview_button, 0.3)
	


func hover_anim(target: Control, desired_scale: Vector2):
	if scale_tw != null: scale_tw.kill()
	scale_tw = create_tween()
	if scale_tw == null : return
	
	scale_tw.set_trans(hover_anim_trans_type)
	
	scale_tw.tween_property(target, "scale", desired_scale, hover_anim_duration)




func on_mouse_entered() -> void:
	current_state = States.HOVER


func on_mouse_exited() -> void:
	if click_signals_button.has_focus() : return
	current_state = States.NORMAL


func on_pressed() -> void:
	current_state = States.CLICK
