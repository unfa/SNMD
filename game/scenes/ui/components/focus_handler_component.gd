extends Node
class_name FocusHandlerComponent

@export var sidepanel_nodes : Array[Control]

@onready var menu_content_manager: MenuContentManager = $"../MenuContentPanel/MenuContentViewport"

var last_focused_side_control : Control


func _ready() -> void:
	for node in sidepanel_nodes:
		node.focus_entered.connect(adjust_focus.bind(node))

	menu_content_manager.content_changed.connect(adjust_focus.bind(sidepanel_nodes[0]))
	#adjust_focus(menu_content_manager.play_menu_content, )


func _input(event: InputEvent) -> void:
	if event.is_action_pressed("ui_back"):
		last_focused_side_control.grab_focus()


func adjust_focus(last_focus_node : Control) -> void:
	var content : MenuContent = menu_content_manager.node_state(menu_content_manager.selected)
	for node in sidepanel_nodes:
		if is_instance_valid(content.first_focus_node):
			node.focus_neighbor_right = content.first_focus_node.get_path()

	change_last_focused_side_control(content, last_focus_node)


func change_last_focused_side_control(content : MenuContent, last_focus_node : Control):
	last_focused_side_control = last_focus_node
	for node in content.nodes_focus_to_sidebuttons:
		node.focus_neighbor_left = last_focus_node.get_path()


