extends Node
class_name PauseMenuComponent

@export var transition_duration : float = 0.5
@export var transition_type : Tween.TransitionType = Tween.TRANS_CUBIC

@onready var menu : MainMenu = get_parent()

var fade_tw : Tween

var menu_visible : bool = false :
	set(value):
		if menu_visible == value : return
		if(value):
			get_tree().paused = true
			Input.mouse_mode = Input.MOUSE_MODE_VISIBLE
			fade_in()
		else:
			get_tree().paused = false
			Input.mouse_mode = Input.MOUSE_MODE_CAPTURED
			fade_out()

		menu_visible = value


func _input(event):
	if Input.is_action_just_pressed("ui_cancel"):
		menu_visible = !menu_visible
		get_viewport().set_input_as_handled()


func _ready() -> void:
	if not is_instance_valid(menu):
		self.queue_free()
		return

	await menu.ready
	menu.resume_button.pressed.connect(func(): menu_visible = false)


func fade_out():
	if fade_tw != null : fade_tw.kill()
	fade_tw = create_tween().set_trans(transition_type)

	menu.visible = true
	fade_tw.tween_property(menu, "modulate", Color(1, 1, 1, 0), transition_duration/2)
	fade_tw.tween_callback(func() : menu.visible = false; menu.get_node("%LoadingScreen").hide())


func fade_in():
	if fade_tw != null : fade_tw.kill()
	fade_tw = create_tween().set_trans(transition_type)

	menu.visible = true

	fade_tw.tween_property(menu, "modulate", Color(1, 1, 1, 1), transition_duration/2)
