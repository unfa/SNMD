extends HBoxContainer
class_name OptionSlider


enum Types { NORMAL, AUDIO }

@export var type : Types = Types.NORMAL
@export var save_name : String = ""

@export_category("Range")
@export var min_value : float = 0
@export var max_value : float = 1
@export var step : float = 0.01
@export var page : float = 0
@export var exp_edit : bool = false
@export var rounded : bool = false
@export var allow_greater : bool = false
@export var allow_lesser : bool = false

@onready var slider: HSlider = $HSlider
@onready var value_spin_box: SpinBox = $ValueSpinBox


func _ready() -> void:
	sync_ranges()
	Input


func sync_ranges():
	for range in [slider, value_spin_box]:
		range.min_value = min_value
		range.max_value = max_value
		range.step = step
		range.page = page
		range.exp_edit = exp_edit
		range.rounded = rounded
		range.allow_greater = allow_greater
		range.allow_lesser = allow_lesser
	
	match type:
		Types.AUDIO: 
			value_spin_box.max_value = 100
			value_spin_box.step = 1


func on_slider_value_changed(value: float) -> void:
	match type:
		Types.AUDIO : value_spin_box.value = int(value * 100)
		_: value_spin_box.value = value



func on_value_spin_box_value_changed(value: float) -> void:
	match type:
		Types.AUDIO : slider.value = value / 100
		_: slider.value = value
