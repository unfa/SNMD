extends HBoxContainer
class_name OptionList

enum Types {RESOLUTION, OVERALL_GRAPHICS, SCALING_MODE, WINDOW_MODE, NORMAL}

@export var items : Array

@export var type : Types = Types.NORMAL
@export var save_name : String = ""

@onready var option_button: OptionButton = $OptionButton


func _ready() -> void:
	assign_items()



func assign_items():
	match type:
		Types.OVERALL_GRAPHICS : items = CommonData.OverallGraphics.keys()
		Types.RESOLUTION: items = CommonData.Resolutions.keys()
		Types.SCALING_MODE: items = CommonData.ScalingModes.keys()
		Types.WINDOW_MODE: items = CommonData.WindowModes.keys()
	
	for item in items:
		option_button.add_item(item)


func select_with_text(text : String):
	option_button.select(items.find(text))

