extends CenterContainer
class_name LevelSelectTile

@onready var level_preview_button: TextureButton = $LevelPreviewPanel/LevelPreviewButton
@onready var label: Label = $Label
@onready var pause_menu_scene = load("res://scenes/main_menu/pause_menu.tscn")

var level_scene : PackedScene

var level_metadata : LevelMetadata:
	set(value):
		level_metadata = value
		await ready
		load_metadata()

var level_scene_path : String
var level_image_path : String

func load_metadata():
	print("Level loading metadata")
	label.text = level_metadata.level_name
	load_image()


func load_image():
	var level_image := Image.new()
	print("Tile loading image from file path: ", level_image_path)
	level_image.load(level_image_path)
	print("Tile loaded image: ", level_image)

	if not is_instance_valid(level_image):
		push_error("Tile tried to load image, but image is ", level_image)
		return

	level_preview_button.texture_normal = ImageTexture.create_from_image(level_image)


func get_level_sha256(level_scene_path):
	var sha256 = FileAccess.get_sha256(level_scene_path)
	print("Got level sha256: ", sha256)
	return sha256


func load_level_scene():
	level_scene = load(level_scene_path)

	if not is_instance_valid(level_scene):
		push_error("Tile tried to load level scene, but level scene is ", level_scene)


func on_level_select_pressed() -> void:
	var main_menu = get_tree().root.get_node_or_null("Menu")

	var pause_menu_instance = get_tree().root.get_node_or_null("PauseMenu")

	if not is_instance_valid(pause_menu_instance):
		pause_menu_instance = pause_menu_scene.instantiate()
		get_tree().root.add_child(pause_menu_instance)
		await get_tree().process_frame

	pause_menu_instance.show_loading_screen()
	await get_tree().process_frame

	var old_level = get_tree().root.get_node_or_null("Level")
	if is_instance_valid(old_level):
		old_level.name = "old_Level"
		old_level.queue_free()
		await get_tree().process_frame

	var level_sha256 = get_level_sha256(level_scene_path)
	load_level_scene()
	var level_instance = level_scene.instantiate()
	level_instance.level_sha256 = level_sha256
	get_tree().root.add_child(level_instance)

	if is_instance_valid(pause_menu_instance):
		pause_menu_instance.hide_pause_menu()

	await get_tree().process_frame

	if is_instance_valid(main_menu):
		main_menu.queue_free()

	get_tree().paused = false
	MusicManager.play()

	level_preview_button.release_focus()
