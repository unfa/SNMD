extends Control
class_name MenuContent

@export var first_focus_node : Control
@export var nodes_focus_to_sidebuttons : Array[Control] #Nodes that can focus back to sidepanel
