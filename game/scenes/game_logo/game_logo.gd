extends Node3D

@onready var vport : Viewport = get_viewport()

var image_path = "res://scenes/game_logo/game_logo.png"

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	# wait for various effects to settle down, reflection probes to get baked etc.
	await create_tween().tween_interval(1).finished

	var image = vport.get_texture().get_image()
	image.resize(1280, 720, Image.INTERPOLATE_LANCZOS)
	image.save_png(image_path)

	prints("Logo render saved to", image_path)

	get_tree().quit()
